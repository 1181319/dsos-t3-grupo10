const mysql = require("mysql");
const express = require("express");
var app = express();
const bodyparser = require("body-parser");

app.use(bodyparser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});
var mysqlConnection = mysql.createConnection({
    host: "ctesp.dei.isep.ipp.pt",
    user: "grupo10_dsos",
    password: "qwerty12345",
    database: "grupo10_dsos",
    multipleStatements:true


});

mysqlConnection.connect((err) => {
    if (!err)
        console.log('Connexão à BD com sucesso!');
    else
        console.log('Ligação à BD falhou! Erro : ' + JSON.stringify(err, undefined, 2));

});


app.listen(3000, () => console.log("Servidor Express a correr na porta 3000"));

//Vai buscar todos os utentes
app.get("/Utentes", (req, res) => {
    mysqlConnection.query('Select * from Utente', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});



//Eliminar Utentes
app.delete("/Utentes/:id", (req, res) => {
    mysqlConnection.query('Delete from Utente where IdUtente = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Utente eliminado ');
        else
            console.log(err);
    })
});

//Vai buscar utente pelo ID
app.get("/Utentes/:id", (req, res) => {
    mysqlConnection.query('Select * from Utente where IDUtente = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Vai buscar terapeutica pelo ID
app.get("/Terapeuticas/:id", (req, res) => {
    mysqlConnection.query('Select Utente.Nome as Nome,IdTerapeutica,Duracao,(case when Validada=0 then "Não" else "Sim" end) as Validada,SUBSTRING(convert(DataTerapeutica,DATE),1,10) as DataTerapeutica from Terapeutica join Utente on Terapeutica.IdUtente=Utente.IdUtente where IDTerapeutica = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//Linhas de terapeutica
app.get("/Terapeuticas_Linhas/:id", (req, res) => {
    mysqlConnection.query('Select IDTerapeutica_Linhas,IdTerapeutica,Medicamento.Nome as Medicamento,Terapeutica_Linhas.Dosagem as Dosagem,Descricao from Terapeutica_Linhas join Medicamento on Terapeutica_Linhas.IdMedicamento=Medicamento.IdMedicamento where IDTerapeutica = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//Todas terapeuticas
app.get("/Terapeuticas", (req, res) => {
    mysqlConnection.query('Select Utente.Nome as Nome,IdTerapeutica,Duracao,(case when Validada=0 then "Não" else "Sim" end) as Validada,SUBSTRING(convert(DataTerapeutica,DATE),1,10) as DataTerapeutica from Terapeutica join Utente on Terapeutica.IdUtente=Utente.IdUtente', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

//Eliminar terapeuticas
app.delete("/Terapeuticas/:id", (req, res) => {
    mysqlConnection.query('Delete from Terapeutica where IdTerapeutica = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Terapeutica eliminada ');
        else
            console.log(err);
    })
   
});
//Apaga linha terapeuticas
app.delete("/Terapeuticas_Linhas/:id", (req,res) => {
	 mysqlConnection.query('Delete from Terapeutica_Linhas where IdTerapeutica = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Linhas terapeutica eliminadas');
        else
            console.log(err);
    })
});

app.get("/Medicamentos", (req, res) => {
    mysqlConnection.query('Select * from Medicamento', (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});
//Alterar estado de um medicamento
app.put("/Medicamentos_Autorizado/:id", (req, res) => {
    
    mysqlConnection.query('Update Medicamento SET Estado = "Autorizado" where IdMedicamento=? ', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Estado Alterado!');
        else
            console.log(err);
    })
});

app.put("/Medicamentos_Caducado/:id", (req, res) => {
    
    mysqlConnection.query('Update Medicamento SET Estado = "Caducado" where IdMedicamento=? ', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send('Estado Alterado!');
        else
            console.log(err);
    })
});
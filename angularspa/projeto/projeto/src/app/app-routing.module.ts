import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TerapeuticasComponent } from './terapeuticas/terapeuticas.component';
import { UtentesComponent } from './utentes/utentes.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MedicamentosService } from './medicamentos.service';
import { MedicamentosComponent } from './medicamentos/medicamentos.component';
import {TerapeuticaDetalheComponent} from './terapeutica-detalhe/terapeutica-detalhe.component';

const routes: Routes = [
  { path : '', redirectTo: '/utentes', pathMatch : 'full'},
  { path :'terapeuticas', component : TerapeuticasComponent},
  {path : 'utentes', component: UtentesComponent },
  {path : 'medicamentos', component: MedicamentosComponent },
  {path : 'terapeuticadetalhe/:id', component: TerapeuticaDetalheComponent },
  { path :"**",component: PageNotFoundComponent}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [TerapeuticasComponent,
                                  UtentesComponent,
                                  PageNotFoundComponent]
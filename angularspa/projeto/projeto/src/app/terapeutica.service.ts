import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ITerapeuticas } from './terapeuticas/terapeutica';
import { tap,delay,take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TerapeuticaService {

  private  API : string = "http://localhost:3000/Terapeuticas";
  private  API_de_linhas : string = "http://localhost:3000/Terapeuticas_Linhas"
  constructor(private http: HttpClient) { }

 list(){
   return this.http.get<ITerapeuticas[]>(this.API)
   .pipe(
     tap(console.log)
   );
 }

 listid(IdTerapeutica){
   console.log(IdTerapeutica);
   return this.http.get(`${this.API}/${IdTerapeutica}`).pipe(tap(console.log));
 
 }

listlinhas(IdTerapeutica){
  return this.http.get(`${this.API_de_linhas}/${IdTerapeutica}`).pipe(tap(console.log));
}


  remove(IdTerapeutica){
    return this.http.delete(`${this.API}/${IdTerapeutica}`).pipe(take(1));
  }
  removeLinhas(IdTerapeutica){
    return this.http.delete(`${this.API_de_linhas}/${IdTerapeutica}`).pipe(take(1));
  }
}

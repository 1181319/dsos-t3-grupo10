export interface ITerapeuticas {
    IdUtente : number;
    Nome :string;
    IdTerapeutica : number;
    Duracao : string;
    Validada : string;
    DataTerapeutica : Date;
}
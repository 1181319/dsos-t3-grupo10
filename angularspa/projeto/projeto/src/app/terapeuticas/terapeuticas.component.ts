import { Component, OnInit, ViewChild } from '@angular/core';
import { ITerapeuticas } from './terapeutica';
import { TerapeuticaService } from '../terapeutica.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { AlertModalComponent } from '../shared/alert-modal/alert-modal.Component';
import { AlertModalService } from '../shared/alert-modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, empty, of, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import 'Datatables.net';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-terapeuticas',
  template: `
  <div class="card mt-3">
  <div class="card-header">
    <div class="float-left">
      <h4>Terapeuticas</h4>
    </div>
    <div class="float-right">
    <button type="button" class="btn btn-primary" (click)="ajuda()">Ajuda</button> 
      <button type="button" class="btn btn-secondary" (click)="onRefresh2()">Atualizar</button>
    </div>
  </div>
  <div class="card-body">

    <h5>Procura por Utente | Data (YYYY-MM-DD)</h5>
    <div class="table-responsive">
    <table class="table table-bordered" *ngIf="(terapeuticas$ | async) as terapeuticas; else loading" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
            <th>#</th>
            <th>Utente</th>
            <th>Duracao</th>
            <th>Validada</th>
            <th>Data</th>
            <th>Consultar</th>
            <th>Apagar</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
            <th>#</th>
            <th>Utente</th>
            <th>Duracao</th>
            <th>Validada</th>
            <th>Data</th>
            <th>Consultar</th>
            <th>Apagar</th>
            </tr>
        </tfoot>
        <tbody>
        <tr *ngFor="let ITerapeuticas of terapeuticas">
          <td>{{ ITerapeuticas.IdTerapeutica }}</td>  
          <td>{{ ITerapeuticas.Nome }}</td>
          <td>{{ ITerapeuticas.Duracao }}</td>
          <td>{{ ITerapeuticas.Validada }}</td>
          <td>{{ ITerapeuticas.DataTerapeutica }}</td>
          <td>
          <span class="float-right">
         

            <button class="btn btn-outline-primary mb-1 btn-sm" (click)="navega(ITerapeuticas.IdTerapeutica)">
              Consultar
              
            </button>
          </span>
        </td>
          <td>
            <span class="float-right">
           

              <button class="btn btn-outline-danger mb-1 btn-sm" (click)="onDelete(ITerapeuticas)">
                Remover
              </button>
            </span>
          </td>
        </tr>
      </tbody>
    </table>
</div>


    <ng-template #loading>
      <span>Carregando Terapeuticas...</span>
    </ng-template>

    <!--ng-template #loadingError>
      <div *ngIf="error$ | async; else loading" >
        Erro ao carregar Terapeuticas. Tente novamente mais tarde.
      </div>
      <ng-template #loading>
          <span>Carregando Terapeuticas...</span>
      </ng-template>
    </ng-template -->
  </div>
</div>

<ng-template #deleteModal>
  <div class="modal-body text-center">
    <p>Tem certeza que deseja remover essa terapeutica?</p>
    <button type="button" class="btn btn-default" (click)="onConfirmDelete()">Sim</button>
    <button type="button" class="btn btn-primary" (click)="onDeclineDelete()">Nao</button>
  </div>
</ng-template>
 
  `,
  styles: []
})
export class TerapeuticasComponent implements OnInit {

  terapeuticas : ITerapeuticas[];
  terapeuticas$: Observable<ITerapeuticas[]>;
  constructor(private _TerapeuticasService: TerapeuticaService,
              private modalService: BsModalService,
              private alertService: AlertModalService,
              private router : Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this._TerapeuticasService.list()
    .subscribe(dados => this.terapeuticas = dados);
    this.onRefresh();
  }

  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal', { static: true }) deleteModal;

  terapeuticaSelecionada : ITerapeuticas;

  onDelete(ITerapeuticas) {
    this.terapeuticaSelecionada = ITerapeuticas;
    this.deleteModalRef = this.modalService.show( this.deleteModal, { class: 'modal-sm'});
  }

  onConfirmDelete(){
    this._TerapeuticasService.remove(this.terapeuticaSelecionada.IdTerapeutica)
      .subscribe(
        success => {
        
          this.onRefresh2();
          this.deleteModalRef.hide();
          this.alertService.showAlertDanger('A terapeutica foi elimindada.');
        },
        error => {
          this.onRefresh2();
          this.deleteModalRef.hide();
          this.alertService.showAlertDanger('A terapeutica foi eliminada.');
        }
      );
      this._TerapeuticasService.removeLinhas(this.terapeuticaSelecionada.IdTerapeutica).subscribe(
        success => {
          
        },
        error=>{

        }
      );
  }

  onDeclineDelete(){
    this.deleteModalRef.hide();

  }

  ajuda(){
    this.alertService.showConfirm('Ajuda','Esta área permite a filtrar a lista de terapeuticas, procurando por nome do utente, ou por data. Caso caixa de procura não apareça, clique em Atualizar!','OK!')
  }
  onRefresh2() {
      
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['terapeuticas']);
      setTimeout(function(){  $('#dataTable').DataTable({paging:false}); }, 500);
  }); 
  };

  onRefresh() {
    this.terapeuticas$ = this._TerapeuticasService.list().pipe(
      // map(),
      // tap(),
      // switchMap(),
      catchError(error => {
        console.error(error);
        // this.error$.next(true);
        this.handleError();
        return empty();
      })
    );

}

consulta(){
  this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
    this.router.navigate(['terapeuticadetalhe']);
    setTimeout(function(){  $('#dataTable').DataTable({paging:false}); }, 500);
}); 

}

handleError() {
  this.alertService.showAlertDanger('Erro ao carregar terapeuticas. Tente novamente mais tarde.');
  // this.bsModalRef = this.modalService.show(AlertModalComponent);
  // this.bsModalRef.content.type = 'danger';
  // this.bsModalRef.content.message = 'Erro ao carregar cursos. Tente novamente mais tarde.';
}

navega(IdTerapeutica){
  this.router.navigate(['/terapeuticadetalhe',IdTerapeutica]);
}  

}

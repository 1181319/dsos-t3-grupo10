import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerapeuticasComponent } from './terapeuticas.component';

describe('TerapeuticasComponent', () => {
  let component: TerapeuticasComponent;
  let fixture: ComponentFixture<TerapeuticasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerapeuticasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerapeuticasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

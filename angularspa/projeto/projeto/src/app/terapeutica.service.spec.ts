import { TestBed } from '@angular/core/testing';

import { TerapeuticaService } from './terapeutica.service';

describe('TerapeuticaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TerapeuticaService = TestBed.get(TerapeuticaService);
    expect(service).toBeTruthy();
  });
});

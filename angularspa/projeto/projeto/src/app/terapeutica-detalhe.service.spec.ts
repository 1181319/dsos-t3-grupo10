import { TestBed } from '@angular/core/testing';

import { TerapeuticaDetalheService } from './terapeutica-detalhe.service';

describe('TerapeuticaDetalheService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TerapeuticaDetalheService = TestBed.get(TerapeuticaDetalheService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild } from '@angular/core';
import { UtenteService } from '../utente.service';
import { IUtente } from './utente';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { AlertModalComponent } from '../shared/alert-modal/alert-modal.Component';
import { AlertModalService } from '../shared/alert-modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, empty, of, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import 'Datatables.net';

declare var jquery: any;
declare var $: any;




@Component({
  selector: 'app-utentes',
  template: `
   
  <div class="card mt-3">
  <div class="card-header">
    <div class="float-left">
      <h4>Utentes</h4>
    </div>
    <div class="float-right">
    <button type="button" class="btn btn-primary" (click)="ajuda()">Ajuda</button> 
      <button type="button" class="btn btn-secondary" (click)="onRefresh2()">Atualizar</button> 

    </div>
  </div>
  <div class="card-body">

  <h5>Procura por Nome | Idade | Morada </h5>

    <div class="table-responsive">
    <table class="table table-bordered" *ngIf="(utentes$ | async) as utentes; else loading" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Idade</th>
                <th>Morada</th>
                <th>Apagar</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Idade</th>
                <th>Morada</th>
               <th>Apagar</th>
            </tr>
        </tfoot>
        <tbody>
        <tr *ngFor="let IUtente of utentes">
          <td>{{ IUtente.IdUtente }}</td>
          <td>{{ IUtente.Nome }}</td>
          <td>{{ IUtente.Idade }}</td>
          <td>{{ IUtente.Morada }}</td>
          <td>
            <span class="float-right">
              

              <button class="btn btn-outline-danger mb-1 btn-sm" (click)="onDelete(IUtente)">
                Eliminar
              </button>
            </span>
          </td>
        </tr>
      </tbody>
    </table>
</div>
    

    <ng-template #loading>
      <span>Carregando Utentes...</span>
    </ng-template>

    <!--ng-template #loadingError>
      <div *ngIf="error$ | async; else loading" >
        Erro ao carregar cursos. Tente novamente mais tarde.
      </div>
      <ng-template #loading>
          <span>Carregando Cursos...</span>
      </ng-template>
    </ng-template -->
  </div>
</div>

<ng-template #deleteModal>
  <div class="modal-body text-center">
    <p>Tem certeza que deseja remover esse utente?</p>
    <button type="button" class="btn btn-default" (click)="onConfirmDelete()">Sim</button>
    <button type="button" class="btn btn-primary" (click)="onDeclineDelete()">Nao</button>
  </div>
</ng-template>



  `,
  styles: []
})
export class UtentesComponent implements OnInit {
  
  utentes : IUtente[];
  utentes$: Observable<IUtente[]>;
  
  constructor(private _UtentesService: UtenteService,
    private modalService: BsModalService,
              private alertService: AlertModalService,
              private router: Router,
              private route: ActivatedRoute
              ) { }

  ngOnInit() {
    this._UtentesService.list()
    .subscribe(dados => this.utentes = dados);
    this.onRefresh();
    
  }

  
  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal', { static: true }) deleteModal;

  utenteSelecionado : IUtente;

  onDelete(IUtente) {
    this.utenteSelecionado = IUtente;
    this.deleteModalRef = this.modalService.show( this.deleteModal, { class: 'modal-sm'});
  }

  onConfirmDelete(){
    this._UtentesService.remove(this.utenteSelecionado.IdUtente)
      .subscribe(
        success => {
        
          this.onRefresh2();
          this.deleteModalRef.hide();
          this.alertService.showAlertSuccess('O Utente foi eliminado.');
         
        },
        error => {
          this.onRefresh2();
          this.deleteModalRef.hide();
          this.alertService.showAlertSuccess('O Utente foi eliminado.');
          
        }
      );
  }

  onDeclineDelete(){
    this.deleteModalRef.hide();

  }

  ajuda(){
    this.alertService.showConfirm('Ajuda','Esta área permite a filtrar a lista de utentes, procurando por nome, idade ou morada. Caso caixa de procura não apareça, clique em Atualizar!','OK!')
  }
  onRefresh2() {
      
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['utentes']);
      setTimeout(function(){  $('#dataTable').DataTable({paging:false}); }, 500);
  }); 
  };

  
  onRefresh() {
    this.utentes$ = this._UtentesService.list().pipe(
      // map(),
      // tap(),
      // switchMap(),
      catchError(error => {
        console.error(error);
        // this.error$.next(true);
        this.handleError();
        return empty();
      })
    );
    }
   



handleError() {
  this.alertService.showAlertDanger('Erro ao carregar utentes. Tente novamente mais tarde.');
  // this.bsModalRef = this.modalService.show(AlertModalComponent);
  // this.bsModalRef.content.type = 'danger';
  // this.bsModalRef.content.message = 'Erro ao carregar cursos. Tente novamente mais tarde.';
}

}
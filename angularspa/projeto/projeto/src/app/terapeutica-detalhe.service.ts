import { Injectable } from '@angular/core';

import { IUtente } from './utentes/utente';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap,delay,take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UtenteService {

  private readonly API : string = "http://localhost:3000/Utentes";
  constructor(private http: HttpClient) { }

 list(){
   return this.http.get<IUtente[]>(this.API)
   .pipe(
     tap(console.log)
   );
 }

  remove(IdUtente){
    return this.http.delete(`${this.API}/${IdUtente}`).pipe(take(1));
    
  }


  
}

export interface ITerapeuticaDetalhe {
    IdTerapeutica_Linhas : number;
    IdTerapeutica :number;
    Medicamento : string;
    Dosagem : string;
    Descricao : Date;
}
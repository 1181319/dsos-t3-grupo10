import { Component, OnInit, ViewChild } from '@angular/core';
import { ITerapeuticas } from '../terapeuticas/terapeutica';
import { TerapeuticaService } from '../terapeutica.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { AlertModalComponent } from '../shared/alert-modal/alert-modal.Component';
import { AlertModalService } from '../shared/alert-modal.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, empty, of, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';
import 'Datatables.net';
import { ITerapeuticaDetalhe } from './terapeuticadetalhe';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-terapeuticasdetalhe',
  template: `
  <div class="card mt-3">
  <div class="card-header">

  <!-- Page Heading -->
  <h5>Dados da Terapêutica      
 <br>
 <br>

  <br>
  <div class="row">

      <form class="needs-validation" novalidate="" style="margin-left:30px;" *ngFor="let ITerapeuticas of terapeuticas">
          <div class="row">
          
              <div class="col-lg-12 mb-3" style="width:400px;">
                  <label for="firstName">Nome</label>
                  <input type="text" class="form-control" id="firstName" placeholder="" value="{{ITerapeuticas.Nome}}" required="" readonly>

              </div>
          </div>
          <div class="row">
              <div class="col-lg-12 mb-3">
                  <label for="address">Duração</label>
                  <input type="text" class="form-control" id="address" placeholder="" value="{{ITerapeuticas.Duracao}}" required="" readonly>

              </div>
          </div>

          <div class="row">
          <div class="col-lg-12 mb-3">
              <label for="address">Data</label>
              <input type="text" class="form-control" id="address" placeholder="" value="{{ITerapeuticas.DataTerapeutica}}" required="" readonly>

          </div>
      </div>

      </form>
  </div>

  <br>
  <h5>Medicamentos da Terapêutica</h5>
  <div class="card shadow mb-4">
      <div class="card-body">
          <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                      <tr>
                          <th>Medicamento</th>
                          <th>Dosagem</th>
                          <th>Descrição</th>
                      </tr>
                  </thead>
                  
                  <tbody>
                  <tr *ngFor="let ITerapeuticaDetalhe of terapeuticasLinhas" >
                  <td style="font-size:11;">{{ ITerapeuticaDetalhe.Medicamento }}</td>  
                  <td style="font-size:11;">{{ ITerapeuticaDetalhe.Dosagem }}</td>
                  <td style="font-size:11;">{{ ITerapeuticaDetalhe.Descricao }}</td>
                
                </tr>
                  </tbody>
              </table>
          </div>
      </div>
  </div>

  <br>  

  
  <button type="button" class="btn btn-primary" (click)="voltar()"> 🡄 Voltar </button> 



 
  `,
  styles: []
})

export class TerapeuticaDetalheComponent implements OnInit {
  data: any;
  id : any;
  terapeuticas : ITerapeuticas[];
  terapeuticasLinhas : ITerapeuticaDetalhe[];
  terapeuticas$: Observable<ITerapeuticas[]>;
  terapeuticasDetalhe$: Observable<ITerapeuticaDetalhe[]>;
  constructor(private _TerapeuticasService: TerapeuticaService,
              private modalService: BsModalService,
              private alertService: AlertModalService,
              private router : Router,
              private route: ActivatedRoute) { }

 
    ngOnInit() {
      this.id=this.route.snapshot.params['id'];
      this._TerapeuticasService.listid(this.id)
      .subscribe(dados => this.terapeuticas = dados);

      this._TerapeuticasService.listlinhas(this.id)
      .subscribe(dados2 => this.terapeuticasLinhas = dados2);
    }
  

  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal', { static: true }) deleteModal;


  onRefresh2() {
      
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['terapeuticas']);
      setTimeout(function(){  $('#dataTable').DataTable({paging:false}); }, 500);
  }); 
  };

  onRefresh() {
    this.terapeuticas$ = this._TerapeuticasService.list().pipe(
      // map(),
      // tap(),
      // switchMap(),
      catchError(error => {
        console.error(error);
        // this.error$.next(true);
        this.handleError();
        return empty();
      })
    );

}

handleError() {
  this.alertService.showAlertDanger('Erro ao carregar terapeuticas. Tente novamente mais tarde.');
  // this.bsModalRef = this.modalService.show(AlertModalComponent);
  // this.bsModalRef.content.type = 'danger';
  // this.bsModalRef.content.message = 'Erro ao carregar cursos. Tente novamente mais tarde.';
}

voltar(){
  this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
    this.router.navigate(['terapeuticas']);
    setTimeout(function(){  $('#dataTable').DataTable({paging:false}); }, 500);
}); 
}


}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TerapeuticaDetalheComponent } from './terapeutica-detalhe.component';

describe('TerapeuticaDetalheComponent', () => {
  let component: TerapeuticaDetalheComponent;
  let fixture: ComponentFixture<TerapeuticaDetalheComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TerapeuticaDetalheComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TerapeuticaDetalheComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

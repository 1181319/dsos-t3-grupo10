import { Injectable } from '@angular/core';
import { IMedicamentos } from './medicamentos/medicamentos';
import { HttpClient } from '@angular/common/http';
import { tap,delay,take,map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MedicamentosService {

  private readonly API : string = "http://localhost:3000/Medicamentos";
  private   API2 : string ="http://localhost:3000/Medicamentos_Autorizado";
  private API3 : string ="http://localhost:3000/Medicamentos_Caducado";
  constructor(private http: HttpClient) { }

  

 list(){
   return this.http.get<IMedicamentos[]>(this.API)
   .pipe(
     tap(console.log)
   );
 }

estadoAutorizado(IdMedicamento){
  
 return  this.http.put(this.API2 +'/' +IdMedicamento, null).pipe(take(1));
                    
}

estadoCaducado(IdMedicamento) {
 
  return  this.http.put(this.API3 +'/' +IdMedicamento, null).pipe(take(1));
}

}
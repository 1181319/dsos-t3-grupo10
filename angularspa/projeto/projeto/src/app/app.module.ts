import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { UtenteService } from './utente.service';
import { SharedModule } from './shared/shared.module';
import { MedicamentosComponent } from './medicamentos/medicamentos.component';
import { TerapeuticaDetalheComponent } from './terapeutica-detalhe/terapeutica-detalhe.component';

@NgModule({
  declarations: [
    AppComponent,
 routingComponents,
 PageNotFoundComponent,
 MedicamentosComponent,
 TerapeuticaDetalheComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ModalModule.forRoot(),
    SharedModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

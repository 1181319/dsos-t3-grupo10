import { Component, OnInit, ViewChild } from '@angular/core';
import { IMedicamentos } from './medicamentos';

import { MedicamentosService } from '../medicamentos.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { AlertModalComponent } from '../shared/alert-modal/alert-modal.Component';
import { AlertModalService } from '../shared/alert-modal.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Observable, empty, of, Subject, EMPTY } from 'rxjs';
import { catchError, switchMap, take } from 'rxjs/operators';

import 'Datatables.net';

declare var jquery: any;
declare var $: any;


@Component({
  selector: 'app-medicamentos',
  template: `
  <div class="card mt-3">
  <div class="card-header">
    <div class="float-left">
      <h4>Medicamentos</h4>
    </div>
    <div class="float-right">
    <button type="button" class="btn btn-primary" (click)="ajuda()">Ajuda</button> 
      <button type="button" class="btn btn-secondary" (click)="onRefresh2()">Atualizar</button> 

    </div>
  </div>
  <div class="card-body">


    <div class="table-responsive">
    <table class="table table-bordered" *ngIf="(medicamentos$ | async) as medicamentos; else loading" id="dataTable" width="100%" cellspacing="0">
        <thead>
            <tr>
                <th>ID</th>
                <th>Marca</th>
                <th>Nome</th>
                <th>Forma</th>
                <th>Dosagem</th>
                <th>Estado</th>
                <th>Alterar Estado</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
            <th>ID</th>
            <th>Marca</th>
            <th>Nome</th>
            <th>Forma</th>
            <th>Dosagem</th>
            <th>Estado</th>
            <th>Alterar Estado</th>
            </tr>
        </tfoot>
        <tbody>
        <tr *ngFor="let IMedicamentos of medicamentos">
          <td>{{ IMedicamentos.IdMedicamento }}</td>
          <td>{{ IMedicamentos.Marca }}</td>
          <td>{{ IMedicamentos.Nome }}</td>
          <td>{{ IMedicamentos.Forma_Farmaceutica }}</td>
          <td>{{ IMedicamentos.Dosagem}}</td>
          <td>{{ IMedicamentos.Estado }}</td>
          <td>
            <span class="float-right">
              

              <button class="btn btn-outline-warning mb-1 btn-sm" (click)="alterarEstado(IMedicamentos.IdMedicamento,IMedicamentos.Estado)">
                Alterar Estado
              </button>
            </span>
          </td>
        </tr>
      </tbody>
    </table>
</div>
    

    <ng-template #loading>
      <span>Carregando Medicamentos...</span>
    </ng-template>

    <!--ng-template #loadingError>
      <div *ngIf="error$ | async; else loading" >
        Erro ao carregar medicamentos. Tente novamente mais tarde.
      </div>
      <ng-template #loading>
          <span>Carregando medicamentos...</span>
      </ng-template>
    </ng-template -->
  </div>
</div>



  `,
  styles: []
})
export class MedicamentosComponent implements OnInit {

  medicamentos : IMedicamentos[];
  medicamentos$: Observable<IMedicamentos[]>;
  constructor(private _MedicamentosService: MedicamentosService,
              private modalService: BsModalService,
              private alertService: AlertModalService,
              private router : Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this._MedicamentosService.list()
    .subscribe(dados => this.medicamentos = dados);
    this.onRefresh();
  }

  deleteModalRef: BsModalRef;
  @ViewChild('deleteModal', { static: true }) deleteModal;

  medicamentoSelecionado : IMedicamentos;
  estadoSelecionado : IMedicamentos;

  onRefresh() {
    this.medicamentos$ = this._MedicamentosService.list().pipe(
      // map(),
      // tap(),
      // switchMap(),
      catchError(error => {
        console.error(error);
        // this.error$.next(true);
        this.handleError();
        return empty();
      })
    );



}
  ajuda(){
    this.alertService.showConfirm('Ajuda','Esta área permite a filtrar a lista de medicamentos e alterar o seu estado, Caso a caixa de procura não apareça, clique em Atualizar!','OK!')
  }
  onRefresh2() {
      
    this.router.navigateByUrl('/RefreshComponent', { skipLocationChange: true }).then(() => {
      this.router.navigate(['medicamentos']);
      setTimeout(function(){  $('#dataTable').DataTable({paging:false}); }, 500);
  }); 
  };

  alterarEstado(IdMedicamento,Estado){
    this.medicamentoSelecionado= IdMedicamento;
    
    console.log(this.medicamentoSelecionado);
    console.log(Estado);
    if (Estado!="Autorizado") {
        this._MedicamentosService.estadoAutorizado(IdMedicamento)
        .subscribe(
          success => {
          
            this.onRefresh2();
            this.deleteModalRef.hide();
            this.alertService.showAlertSuccess('O Estado foi alterado.');
           
          },
          error => {
            this.onRefresh2();
         
            this.alertService.showAlertSuccess('O Estado foi alterado.');
            
          })
        }else{
        this._MedicamentosService.estadoCaducado(IdMedicamento)
        .subscribe(
          success => {
          
            this.onRefresh2();
            this.deleteModalRef.hide();
            this.alertService.showAlertSuccess('O Estado foi alterado.');
           
          },
          error => {
            this.onRefresh2();
         
            this.alertService.showAlertSuccess('O Estado foi alterado.');
            
          })
    }
  }
handleError() {
  this.alertService.showAlertDanger('Erro ao carregar medicamentos. Tente novamente mais tarde.');
  // this.bsModalRef = this.modalService.show(AlertModalComponent);
  // this.bsModalRef.content.type = 'danger';
  // this.bsModalRef.content.message = 'Erro ao carregar cursos. Tente novamente mais tarde.';
}

  
}

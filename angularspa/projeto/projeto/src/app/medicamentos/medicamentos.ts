export interface IMedicamentos{
    IdMedicamento : number;
    Marca : string;
    Nome: string;
    Forma_Farmaceutica : string;
    Dosagem: string;
    Estado: string;
    Generico: string;
    PrincipioAtivo: string;
}